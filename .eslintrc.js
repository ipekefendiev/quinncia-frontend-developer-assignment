module.exports = {
  parser: 'babel-eslint',
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: ['airbnb'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'import/no-unresolved': 0,
    'no-underscore-dangle': 0,
    'comma-dangle': 0,
    'implicit-arrow-linebreak': 0,
    'function-paren-newline': 0,
    'react/jsx-curly-newline': 0,
    'no-console': 0,
  },
};
