import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Photo from '../components/Photo';
import Comment from '../components/Comment';
import AppLayout from '../layouts/app.layout';
import { createPhotoComment } from '../redux/photo';
import ENDPOINTS from '../constants/endpoints';
import { fetchTags } from '../redux/tag';
import Tag from '../components/Tag';

const Wrapper = styled.div`
  display: flex;
  gap: 64px;
`;

const DetailContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 600px;
  gap: 16px;
`;

const CommentsContainer = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 4px;
  background-color: #eae7e7;
  border-radius: 8px;
  padding: 8px 32px;
`;

const StyledForm = styled.form`
  display: flex;
  align-items: center;
  gap: 4px;
`;

const StyledTextArea = styled.textarea`
  width: 100%;
  padding: 8px;
`;

const Detail = styled.div`
  display: flex;
  flex-direction: column;
  gap: 64px;
`;

const TagsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 4px;
`;

const NewCommentArea = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [commentValue, setCommentValue] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    if (commentValue) {
      dispatch(createPhotoComment(id, commentValue));

      // TODO: Page should not be reloaded. Instead, the photo detail data should be refetched.
      // This is a temporary workaround
      navigate(0);
    }
  };

  return (
    <div>
      <p>Add a Comment</p>
      <StyledForm onSubmit={handleSubmit}>
        <StyledTextArea
          value={commentValue}
          onChange={(e) => setCommentValue(e.target.value)}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              handleSubmit(e);
              setCommentValue('');
            }
          }}
        />
        <button type="submit">Submit</button>
      </StyledForm>
    </div>
  );
};

const Comments = ({ data }) => (
  <DetailContainer>
    <h3>Comments</h3>
    <CommentsContainer>
      {data.commentIDs.length ? (
        data.commentIDs.map((commentId) => (
          <Comment key={commentId} id={commentId} />
        ))
      ) : (
        <p>No comments yet</p>
      )}
    </CommentsContainer>
    <NewCommentArea />
  </DetailContainer>
);

Comments.propTypes = {
  data: PropTypes.shape().isRequired,
};

const Tags = ({ data }) => {
  const tags = useSelector((state) => state.tag);

  const photoTagsArr = useMemo(() => {
    if (data?.tagIDs?.length && tags.length) {
      const photoTags = data.tagIDs.split(',');

      return photoTags.map((tagID) => {
        const foundTag = tags.find((item) => item._id === tagID);

        return { name: foundTag?.name, _id: foundTag?._id };
      });
    }
    return [];
  }, [tags]);

  return (
    <div>
      <h3>Tags</h3>
      <TagsContainer>
        {photoTagsArr?.length ? (
          photoTagsArr.map((tag) => <Tag key={tag._id}>{tag.name}</Tag>)
        ) : (
          <p>No Tags attached to this photo</p>
        )}
      </TagsContainer>
    </div>
  );
};

Tags.propTypes = {
  data: PropTypes.shape().isRequired,
};

const PhotoPage = () => {
  const { id } = useParams();
  const [data, setData] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchPhoto = async () => {
      const response = await fetch(`${ENDPOINTS.photo.base}/${id}`);
      const { photo } = await response.json();

      setData(photo);
    };

    fetchPhoto();
    dispatch(fetchTags);
  }, []);

  return (
    <AppLayout>
      {data ? (
        <Wrapper>
          <Photo data={data} />
          <Detail>
            <Comments data={data} />
            <Tags data={data} />
          </Detail>
        </Wrapper>
      ) : (
        <p>Loading...</p>
      )}
    </AppLayout>
  );
};

export default PhotoPage;
