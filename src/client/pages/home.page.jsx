/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import AppLayout from '../layouts/app.layout';
import Photo from '../components/Photo';
import PhotoUploader from '../components/PhotoUploader';
import TagUploader from '../components/TagUploader';
import { fetchPhotos } from '../redux/photo';
import { fetchTags } from '../redux/tag';
import Tag from '../components/Tag';

const Title = styled.h1`
  margin-top: 16px;
`;

const PhotosContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 32px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 4px;

  & * {
    text-align: center;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

const Uploaders = styled.div`
  display: flex;
  align-items: center;
  gap: 64px;
  flex-wrap: wrap;
  /* 
  @media (min-width: 850px) {
    gap: 16px;
  } */
`;

const StyledInput = styled.input`
  width: 200px;
  padding: 8px 6px;
  border-radius: 4px;
`;

const TagsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 4px;
`;

const Home = () => {
  const [searchValue, setSearchValue] = useState('');
  const [filteredPhotos, setFilteredPhotos] = useState([]);
  const dispatch = useDispatch();
  const photos = useSelector((state) => state.photo);
  const tags = useSelector((state) => state.tag);

  useEffect(() => {
    dispatch(fetchPhotos);
    dispatch(fetchTags);
  }, []);

  useEffect(() => {
    setFilteredPhotos(photos);
  }, [photos]);

  useEffect(() => {
    if (searchValue) {
      const photosClone = [...photos];

      const filteredTagIDs = tags
        .filter((tag) => {
          if (tag.name.includes(searchValue)) {
            return tag;
          }
        })
        .map((item) => item._id);

      const filteredPhotosArray = photosClone.filter((photo) => {
        const photoTagIDs = photo.tagIDs;

        if (photoTagIDs.length) {
          if (filteredTagIDs.some((tagID) => photoTagIDs.includes(tagID))) {
            return photo;
          }
        }
      });

      setFilteredPhotos(filteredPhotosArray);
    } else {
      setFilteredPhotos(photos);
    }
  }, [searchValue]);

  return (
    <AppLayout>
      <Wrapper>
        <Uploaders>
          <PhotoUploader />
          <TagUploader />
        </Uploaders>
        <Title>Photos</Title>
        <div>
          <p>Search photos by tags</p>
          <StyledInput
            placeholder="Type a tag to filter photos"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
          />
        </div>
        <PhotosContainer>
          {filteredPhotos?.length ? (
            filteredPhotos.map((photo) => {
              const photoTagIDsArr = photo.tagIDs?.length
                ? photo.tagIDs.split(',')
                : [];

              return (
                <Container key={photo._id}>
                  <Photo data={photo} />
                  {photoTagIDsArr.length ? (
                    <TagsContainer>
                      {photoTagIDsArr.map((tagID) => (
                        <Tag key={tagID}>
                          {tags.find((tag) => tag._id === tagID)?.name}
                        </Tag>
                      ))}
                    </TagsContainer>
                  ) : (
                    <p>No tags</p>
                  )}
                  <Link to={`/photo/${photo._id}`}>Go to details</Link>
                </Container>
              );
            })
          ) : (
            <p>No photos yet. Upload one!</p>
          )}
        </PhotosContainer>
      </Wrapper>
    </AppLayout>
  );
};

export default Home;
