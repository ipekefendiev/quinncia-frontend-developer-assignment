import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ENDPOINTS from '../constants/endpoints';

const Image = styled.img`
  border: 1px solid gray;
  padding: 8px;
  border-radius: 8px;
  width: 250px;
  height: 300px;
`;

const Photo = ({ data }) => (
  <Image src={`${ENDPOINTS.photo.photoContent}/${data._id}`} alt="" />
);

Photo.propTypes = {
  data: PropTypes.shape().isRequired,
};

export default Photo;
