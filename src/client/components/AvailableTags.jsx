import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import Tag from './Tag';

const TagsContainer = styled.div`
  display: flex;
  gap: 8px;
  max-width: 600px;
`;

const TagsWrapper = styled.div`
  flex-wrap: wrap;
  display: flex;
  gap: 4px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
`;

const AvailableTags = () => {
  const tags = useSelector((state) => state.tag);

  // TODO: Edit photo

  // const addTagToPhoto = async (tagID) => {
  //   fetch(`${ENDPOINTS.photo.base}/${photoID}/tags/attach`, {
  //     method: 'PUT',
  //     body: JSON.stringify({
  //       tagIDs: [tagID],
  //     }),
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //   });
  // };

  return (
    <Wrapper>
      <div>
        <h4>Available Tags</h4>
      </div>
      <TagsContainer>
        {tags.length ? (
          <div>
            <TagsWrapper>
              {tags.map((tag) => (
                <Tag key={tag._id}>{tag.name}</Tag>
              ))}
            </TagsWrapper>
          </div>
        ) : (
          <p>There are no available tags yet. You can add tags.</p>
        )}
      </TagsContainer>
    </Wrapper>
  );
};

export default AvailableTags;
