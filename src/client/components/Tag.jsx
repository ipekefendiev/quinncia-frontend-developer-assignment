import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div`
  padding: 6px 8px;
  border-radius: 8px;
  background-color: #049be5;
  color: white;
  width: fit-content;
  cursor: pointer;
`;

const Tag = ({ children }) => <Container>{children}</Container>;

Tag.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Tag;
