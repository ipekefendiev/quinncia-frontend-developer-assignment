import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import Select from 'react-select';

import { createPhoto } from '../redux/photo';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const OuterContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px;
  border: 1px solid gray;
  border-radius: 8px;
  width: fit-content;
  gap: 4px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 24px;
`;

const PhotoUploader = () => {
  const fileRef = useRef(null);
  const [selectedTags, setSelectedTags] = useState([]);
  const tags = useSelector((state) => state.tag);
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createPhoto(fileRef.current.files[0], selectedTags));
  };

  const handleTagSelect = (tagIDs) => {
    setSelectedTags(tagIDs);
  };

  return (
    <OuterContainer>
      <h4>Upload a photo</h4>
      <Wrapper>
        <Container>
          <form onSubmit={handleSubmit}>
            <input type="file" accept="image/png" ref={fileRef} />
            <button type="submit">Upload</button>
          </form>
        </Container>
      </Wrapper>
      <h4>Choose tags</h4>
      {tags.length ? (
        <Select
          defaultValue={null}
          isMulti
          name="tags"
          options={tags.map((tag) => ({ value: tag._id, label: tag.name }))}
          onChange={(options) =>
            handleTagSelect(options.map((option) => option.value))
          }
        />
      ) : (
        <p>No available tags</p>
      )}
    </OuterContainer>
  );
};

export default PhotoUploader;
