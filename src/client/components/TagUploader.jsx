import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import { createTag } from '../redux/tag';
import AvailableTags from './AvailableTags';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
  padding: 16px;
  border: 1px solid gray;
  border-radius: 8px;
`;

const Input = styled.input`
  padding: 8px;
  border-radius: 8px;
`;

const StyledForm = styled.form`
  display: flex;
  gap: 4px;
`;

const TagUploader = () => {
  const [tagValue, setTagValue] = useState('');

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (tagValue) {
      dispatch(createTag(tagValue));
    }
  };

  return (
    <Container>
      <h4>Submit a Tag to use in photos</h4>
      <StyledForm onSubmit={handleSubmit}>
        <Input
          value={tagValue}
          onChange={(e) => setTagValue(e.target.value)}
          placeholder="Enter tag name"
          onKeyDown={(e) => {
            if (tagValue && e.key === 'Enter') {
              setTagValue('');
              handleSubmit(e);
            }
          }}
        />
        <button type="submit">Add</button>
      </StyledForm>
      <AvailableTags />
    </Container>
  );
};

export default TagUploader;
