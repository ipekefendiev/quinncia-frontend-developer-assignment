import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ENDPOINTS from '../constants/endpoints';

const StyledLi = styled.li`
  padding: 4px 8px;
  border-radius: 8px;

  &:hover {
    background-color: #d9d7d7;
  }
`;

const Comment = ({ id }) => {
  const [data, setData] = useState();

  useEffect(() => {
    const getComment = async () => {
      const res = await fetch(`${ENDPOINTS.comment}/${id}`);
      const { comment: commentData } = await res.json();
      setData(commentData);
    };
    getComment();
  }, []);

  return <StyledLi>{data?.content}</StyledLi>;
};

Comment.propTypes = {
  id: PropTypes.string.isRequired,
};

export default Comment;
