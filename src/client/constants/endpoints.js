import { API_URL } from '.';

// TODO: we can use a proxy instead of concatting API_URL for each endpoint
const ENDPOINTS = {
  photo: {
    base: `${API_URL}/photo`,
    many: `${API_URL}/photo/many`,
    photoContent: `${API_URL}/photo/content`,
  },
  comment: `${API_URL}/comment`,
  tag: `${API_URL}/tag`,
};

export default ENDPOINTS;
