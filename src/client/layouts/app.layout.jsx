import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

const Header = styled.div`
  padding: 16px 32px;
  background-color: #049be5;
`;

const StyledLink = styled(Link)`
  color: white;
  font-size: x-large;
`;

const Content = styled.div`
  padding: 64px;
`;

const AppLayout = ({ children }) => (
  <Layout>
    <Header>
      <StyledLink to="/">Home Page</StyledLink>
    </Header>
    <Content>{children}</Content>
  </Layout>
);

AppLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppLayout;
