import React from 'react';
import { Route, Routes } from 'react-router-dom';

import HomePage from './pages/home.page';
import PhotoPage from './pages/photo.page';

const App = () => (
  <Routes>
    <Route path="/" element={<HomePage />} />
    <Route path="/photo/:id" element={<PhotoPage />} />
  </Routes>
);

export default App;
