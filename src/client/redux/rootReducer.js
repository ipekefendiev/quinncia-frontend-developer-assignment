import { combineReducers } from 'redux';
import photoReducer from './photo';
import tagReducer from './tag';

export default combineReducers({
  photo: photoReducer,
  tag: tagReducer,
});
