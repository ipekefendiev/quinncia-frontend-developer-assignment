import ENDPOINTS from '../constants/endpoints';

const initialState = [];

export default function photoReducer(state = initialState, action) {
  switch (action.type) {
    case 'setPhotos': {
      return action.payload;
    }
    case 'setPhoto': {
      return [...state, action.payload];
    }
    default:
      return state;
  }
}

export async function fetchPhotos(dispatch) {
  const response = await fetch(`${ENDPOINTS.photo.many}`);
  const data = await response.json();

  dispatch({ type: 'setPhotos', payload: data.photos });
}

export function createPhoto(photo, tagIDs) {
  return async function setPhoto(dispatch) {
    const formData = new FormData();
    formData.append('profile', photo);

    if (tagIDs.length) {
      formData.append('tagIDs', tagIDs);
    }

    const response = await fetch(`${ENDPOINTS.photo.base}`, {
      method: 'POST',
      body: formData,
    });
    const { photo: data } = await response.json();

    dispatch({ type: 'setPhoto', payload: data });
  };
}

// TODO: these logics are not related with Redux. Should be separated to another file i.e /utils
export function createPhotoComment(photoID, content) {
  return async function setPhotoComment() {
    fetch(`${ENDPOINTS.comment}`, {
      method: 'POST',
      body: JSON.stringify({
        content,
        photoID,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  };
}
