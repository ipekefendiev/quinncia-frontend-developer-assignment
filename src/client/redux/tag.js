import ENDPOINTS from '../constants/endpoints';

const initialState = [];

export default function tagReducer(state = initialState, action) {
  switch (action.type) {
    case 'setTags': {
      return action.payload;
    }
    case 'setTag': {
      return [...state, action.payload];
    }
    default:
      return state;
  }
}

export async function fetchTags(dispatch) {
  const response = await fetch(`${ENDPOINTS.tag}/many`);
  const { tags } = await response.json();

  dispatch({ type: 'setTags', payload: tags });
}

export function createTag(name) {
  return async function setTag(dispatch) {
    const response = await fetch(`${ENDPOINTS.tag}`, {
      method: 'POST',
      body: JSON.stringify({
        name,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const { tag } = await response.json();

    dispatch({ type: 'setTag', payload: tag });
  };
}
